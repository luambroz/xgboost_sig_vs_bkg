from numpy import loadtxt
from xgboost import XGBClassifier
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score, classification_report
import numpy as np
import pandas as pd
import pickle
import matplotlib.pyplot as plt
import math
import argparse

def main(target, lep_channel, njets, ptv_region, data_path, fold, mBB_max, dRBB_max, max_depth, learning_rate):

  print("====== Training settings ======")
  print(data_path)

  ### Load the DataFrames ###
  if fold == 1:
    DataFrame_train      = pd.read_pickle(data_path + '/' + lep_channel + '_train'      + str(njets) + 'jet' + ptv_region + '_even.pkl')
    DataFrame_validation = pd.read_pickle(data_path + '/' + lep_channel + '_validation' + str(njets) + 'jet' + ptv_region + '_even.pkl')
  elif fold == 2:
    DataFrame_train      = pd.read_pickle(data_path + '/' + lep_channel + '_train'      + str(njets) + 'jet' + ptv_region + '_odd.pkl')
    DataFrame_validation = pd.read_pickle(data_path + '/' + lep_channel + '_validation' + str(njets) + 'jet' + ptv_region + '_odd.pkl')
  else:
    print("Wrong fold.")
    return
  ###########################

  ### Apply training settings ###
  print("====== Training settings ======")
  print("njets: " + str(njets))
  print("ptv_region: " + ptv_region)
  print("fold: " + str(fold))
  print("max_depth: " + str(max_depth))
  print("learning_rate: " + str(learning_rate))

  print("Training Dataframe: ")
  print(DataFrame_train.describe())
  print("Validation Dataframe: ")
  print(DataFrame_validation.describe())
  print("===============================")
  
  if mBB_max:
    print("Number of training events before requiring mBB < " + str(mBB_max) + ": " + str(DataFrame_train.shape[0]))  
    DataFrame_train = DataFrame_train[DataFrame_train["mBB"] < mBB_max]
    DataFrame_validation = DataFrame_validation[DataFrame_validation["mBB"] < mBB_max]
    print("Number of training events before requiring mBB < " + str(mBB_max) + ": " + str(DataFrame_train.shape[0]))
  
  if dRBB_max:
    print("Number of training events before requiring dRBB < " + str(dRBB_max) + ": " + str(DataFrame_train.shape[0]))
    DataFrame_train = DataFrame_train[DataFrame_train["dRBB"] < dRBB_max]
    DataFrame_validation = DataFrame_validation[DataFrame_validation["dRBB"] < dRBB_max]
    print("Number of training events before requiring dRBB < " + str(dRBB_max) + ": " + str(DataFrame_train.shape[0]))
  ##############################

  
  ### Features ###
  if lep_channel == 'ZeroLepton':
    #feature_names = ["mBB", "dRBB", "pTB1", "pTB2", "MET", "dPhiVBB", "dEtaBB", "MEff"] 
    #feature_names = ["mBB", "dRBB", "pTB1", "pTB2", "MET", "dPhiVBB", "dEtaBB", "MEff", "bin_MV2c10B1", "bin_MV2c10B2"] 
    #feature_names = ["mBB", "dRBB", "pTB1", "pTB2", "MET", "dPhiVBB"] 
    #feature_names = ["mBB", "dRBB", "pTB1", "pTB2", "MET", "dPhiVBB", "dEtaBB", "MEff", "nTaus"] 
    #feature_names = ["mBB", "dRBB", "pTB1", "pTB2", "MET", "dPhiVBB", "dEtaBB", "MEff", "softMET"]
    feature_names = ["mBB", "dRBB", "pTB1", "pTB2", "MET", "dPhiVBB", "softMET", "nTaus", "bin_MV2c10B1", "bin_MV2c10B2"]
  elif lep_channel == 'OneLepton':
    feature_names = ["mBB", "dRBB", "pTB1", "pTB2", "pTV", "dPhiVBB", "MET", "dPhiLBmin", "mTW", "dYWH", "Mtop"]
  elif lep_channel == 'TwoLepton':
    feature_names = ["mBB", "dRBB", "pTB1", "pTB2", "pTV", "dPhiVBB", "METSig", "dEtaVBB", "mLL"]
  else:  
    print("Wrong lepton channel.")
    return


  if njets == 3:
    feature_names.append("mBBJ")
    feature_names.append("pTJ3")
    feature_names.append("dRBBJ")
  ######################

  print("Training features (" + str(len(feature_names)) + "):")
  print(DataFrame_train[feature_names].head())
  print("===============================")
  
  ### Convert DataFrames in numpy arrays ###
  X_train         = DataFrame_train[feature_names].values
  Y_train         = DataFrame_train[['is' + target]].values
  X_validation         = DataFrame_validation[feature_names].values
  Y_validation         = DataFrame_validation[['is' + target]].values
  
  weight_scale = 1e7 #To faciliate computation
  X_train_weights =      DataFrame_train[['trainingWeight' + target]].values * weight_scale 
  X_validation_weights = DataFrame_validation[['trainingWeight' + target]].values * weight_scale 
  ##########################################
  
  ### XGB ###
  classifier = "XGB"
  if classifier == "XGB":
  
    model = XGBClassifier(objective='binary:logistic', #Default binary:logistic 
  			                  max_depth = max_depth, 
  			                  n_estimators = 10000,
  			                  learning_rate = learning_rate,
                          feature_names = feature_names,
                          eval_metric = "logloss", 
                          subsample = 0.9, #Default 1
                          colsample_bytree = 0.85, #Default 1
                          tree_method = 'exact',
  			                  nthread = -1)
  
    # Fit the model:
    model.fit(X_train, Y_train.ravel(), 
  	          sample_weight = X_train_weights.ravel(),
              eval_set = [(X_train, Y_train.ravel()), (X_validation, Y_validation.ravel())],
  	          sample_weight_eval_set = [X_train_weights.ravel(), X_validation_weights.ravel()],
              early_stopping_rounds = 30,
              verbose = True) 
  ###########
  
  ### Store the model ###
  f_output = "/data/atlas/atlasdata3/ambroz/VHbb/Models/HybridAbsFSR/training_" + target + "_model_" + classifier + "_logloss" + "_max_depth" + str(max_depth) + "_learning_rate"+ str(learning_rate).replace('.','') + "_" + lep_channel + "_jets" + str(njets) + ptv_region + "_fold" + str(fold)
  
  print(f_output)
  pickle.dump(model, open(f_output + ".dat", "wb"))
  model.save_model(f_output + ".model")
  model.save_model(f_output + ".h5")
  
############

if __name__ == '__main__':

  parser = argparse.ArgumentParser(description = 'MVA Training')

  parser.add_argument('-target', help='Target: VH or VZ', default = 'VH', type = str)
  parser.add_argument('-lep_channel', help='Lepton channel: ZeroLepton, OneLepton, TwoLepton', default = 'ZeroLepton', type = str)
  parser.add_argument('-njets', help='Njets: 2 or 3. Default: 2.', default = 2, type = int)
  parser.add_argument('-ptv',   help='Nothing is the inclusive case, or _ptv_75_150, _ptv_150_250, or _ptv_250. Default: _ptv_250.', default = '_ptv_150_250', type = str)
  parser.add_argument('-data_path', help='Data path.', default = '/data/atlas/atlasdata3/ambroz/VHbb/DataFrames/Hybrid', type = str)
  parser.add_argument('-fold',    help='Set fold. Options are 1 and 2. Default: 1.', default = 1, type = int)
  parser.add_argument('-mBB_max', help='Set mBBMax (unit GeV) e.g: 1000', default = False, type = float)
  parser.add_argument('-dRBB_max', help='Set dRBBMax e.g: 2.5', default = False, type = float)
  parser.add_argument('-max_depth', help='Set max_depth. Default: 3', default = 3, type = int)
  parser.add_argument('-learning_rate', help='Set learning_rate. Default: 0.1', default = 0.1, type = float)

  args = parser.parse_args()

  main(args.target, args.lep_channel, args.njets, args.ptv, args.data_path, args.fold, args.mBB_max, args.dRBB_max, args.max_depth, args.learning_rate)
