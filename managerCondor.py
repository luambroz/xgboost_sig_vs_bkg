import os
import time

#targets = ['VH', 'VZ']
targets = ['VH']
#lep_channels = ['ZeroLepton', 'OneLepton', 'TwoLepton'] 
lep_channels = ['ZeroLepton']
njets = ['2', '3']
ptv_regions = ['_ptv_75_150', '_ptv_150_250', '_ptv_250']
folds = ['1', '2']


max_depths = ['5']
learning_rates = ['0.047']

job_index = 0
for learning_rate in learning_rates:
  for max_depth in max_depths:
    for njet in njets:
      for ptv_region in ptv_regions:
        for fold in folds:
          for target in targets:
            for lep_channel in lep_channels:
              if lep_channel == 'ZeroLepton' and ptv_region == '_ptv_75_150':
                continue
              job_name = 'job_' + str(job_index) 
              f = open(job_name + '.sh', 'w')

              #MVA job
              f.write("#!/bin/bash \n")
              f.write("source /home/ambroz/setup_env.sh \n")
              f.write("cd /home/ambroz/VHbb/XGBoost_Sig_vs_Bkg \n")
              f.write("python MVA.py" + " -target " + target + " -lep_channel " + lep_channel + " -njets " + njet + " -ptv " + ptv_region + " -fold " + fold  + " -max_depth " + max_depth + " -learning_rate " + learning_rate + " \n")
              f.close()
              
              #Condor job with batch settings
              fCondor = open(job_name + '_Condor.sh', "w")
              fCondor.write("executable              = " + job_name + ".sh \n")
              fCondor.write("output                  = output/results.output.$(Cluster) \n")
              fCondor.write("log                     = log/results.log.$(Cluster) \n")
              fCondor.write("stream_output           = True \n")
              fCondor.write("request_cpus            = 4 \n")
              fCondor.write("request_memory          = 16 GB \n")
              fCondor.write("queue \n")
              fCondor.close()
              
              command = "condor_submit " + job_name + "_Condor.sh "
              print(command)              

              time.sleep(1)
              os.system(command)

              job_index+=1


              

