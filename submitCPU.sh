#!/bin/bash

source /home/ambroz/setup_env.sh

echo "Starting!"
START_TIME=$SECONDS

cd /home/ambroz/VHbb/XGBoost_Sig_vs_Bkg

python DataPreprocessingNoNeg.py

ELAPSED_TIME=$(($SECONDS - $START_TIME))
echo "Finished!" 
echo "$(($ELAPSED_TIME/60)) min $(($ELAPSED_TIME%60)) sec" 

#This is a script for Condor
#To send the job: condor_submit submitCondor.sh
