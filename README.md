# Description

Scripts to train a XGBoost classifier for the VH(bb) analysis starting from `ROOT` ntuples. The training can be performed in parallel on a `Condor` batch.
