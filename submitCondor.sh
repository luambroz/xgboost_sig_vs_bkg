executable              = submitCPU.sh
output                  = output/results.output.$(Cluster)
error                   = error/results.error.$(Cluster)
log                     = log/results.log.$(Cluster)
stream_output           = True
request_cpus            = 1
request_memory          = 8 GB
queue
